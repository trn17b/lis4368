# LIS4386- Advanced Web Applications Development

## Troy Noble

### Project #1 Requirements:

*Deliverables*

1. Screenshots of passed and failed validations
2. Screenshot of index.jsp showing updated slides with text/images/links

#### README.md file should include the following items:

* Screenshots of passed and failed validations
* Screenshot of index.jsp showing updated slides with text/images/links
* Course title, your name, and assignment requirements

#### Assignment Screenshots:

*Screenshot of homepage with updated slides*:

![Index page](img/index_splash.png "Homepage for site")

*Screenshot of Successful and Failed validations*

| Successful Validation              | Failed Validation                                 |
| ---------------------------------- | ------------------------------------------------- |
| ![](img/successful_validation.png) | !["Failed validation"](img/failed_validation.png) |


