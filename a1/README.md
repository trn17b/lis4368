# LIS4368 - Advanced Web Application Development

## Troy Noble

### Assignment #1 Requirements:

*Three Parts*

1. Distributed Version Control with Git and Bitbucket
2. Java/SP/Servlet Development Installation
3. Chapter Questions (Chs. 1 - 4)

#### README.md file should include the following items:

* Screenshot of running java Hello (#1 above)

* Screenshot of running http://localhost:9999 (#2 above)

* git commands w/short descriptions

* Bitbucket repo links a) this assignment and b) the completed tutorial above (bitbucketstationlocations)

  

> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git clone - Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![Java](img/helloworld.png "Java running")

*Screenshot of running localhost:9999*

![Tomcat](img/tomcatWorks.png "Tomcat running")

http://localhost:9999/lis4368/

*Screenshot of A1 Webpage* 

![A1 Webpage](img/lis_a1_pg1working.png "A1 Working on localhost")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/trn17b/bitbucketstationlocations/ "Bitbucket Station Locations")

