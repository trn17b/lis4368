# LIS4368 - Advanced Web Application Development

## Troy Noble

### Assignment #2 Requirements:

1. Create the HelloServlet servlet
2. Create the bookstore database servlet

#### README.md file should include the following items:

* Assessment links
* One screenshot of query results from the querybook.html page
* Screenshots of assessment links

#### Assignment Screenshots:

[http://localhost:9999/hello](http://localhost:9999/hello) as the index

![Index home](img/hello_index.png "Hello but the index")

[http://localhost:9999/hello](http://localhost:9999/hello) as the homepage

![Hello home picture](img/hello_home.png "Hello Home")

[http://localhost:9999/hello/HelloHome.html](http://localhost:9999/hello/HelloHome.html)

![HelloHome not index](img/hellohome_notindex.png "HelloHome not index")

[http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello)

![SayHello](img/sayHello.png "Hello, world! servlet")

[http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)

![Querybook](img/querybook.png "Query unselected")

[http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)

![Query bookshop selected](img/querybook_selected.png "Query selected")

**Query results**

![Query results](img/query_result.png "Results")

A2 Webpage

![Index for A2](img/a2_4368.png)