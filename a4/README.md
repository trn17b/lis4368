# LIS4368 - Advanced Web Application Development

## Troy Noble

### Assignment #4 Requirements:

1. Setup server side validation
2. Pass values using servlets
3. Show successful and unsuccessful validations
4. Complete skillsets 10-12

#### README.md file should include the following items:

* Screenshot of successful validation
* Screenshot of unsuccessful validation
* Screenshots of skillsets 10-12

#### Assignment Screenshots:

| Successful                                | Failed                               |
| ----------------------------------------- | ------------------------------------ |
| ![Success](img/successful_validation.png) | ![Failed](img/failed_validation.png) |



*Skillset 10*

![Skillset 10](img/skillset10.png)

*Skillset 11*

![Skillset 11](img/skillset11.png)

*Skillset 12*

| P1                                      | P2                                      |
| --------------------------------------- | --------------------------------------- |
| ![Skillset 12 p1](img/skillset12p1.png) | ![Skillset 12 p2](img/skillset12p2.png) |

