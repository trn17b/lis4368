# LIS4368 - Advanced Web Application Development

## Troy Noble

### Assignment #5 Requirements:

1. Implement insertion functionality to server side validation form
2. Showing of successful insertion
3. Skillsets 13-15

#### README.md file should include the following items:

* Screenshots of successful insertion

* Screenshot of local database after insertion

* Screenshots of skillsets 13-15

  

#### Assignment Screenshots:

| Pre-Insertion                           | Post-Insertion                                         |
| --------------------------------------- | ------------------------------------------------------ |
| ![](img/pre_insert.png "Pre-Insertion") | ![](img/successful_validation_insert.png "Successful") |

*Updated database*

![](img/updated_db.png "Updated database")

###### *Skillsets*

*Skillset 13*

![](img/Skillset13.png "Skillset 13")

*Skillset 14*

![](img/Skillset14.png "Skillset 14")

*Skillset 15*

![](img/Skillset15.png "Skillset 15")