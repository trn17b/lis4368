> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 - Advanced Web Applications Development	

## Troy Noble

**LIS4368 Requirements**

*Course Work Links:*

*Assignments*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations)
    - Provide git command descriptions
    - Install JDK
    - Install Tomcat
    - Provide Screenshots of installations

2. [A2 README.md](a2/README.md)

    - Create HelloServlet Servlet

    - Create Bookstore query database/webpage

    - Provide Screenshots of working pages

3. [A3 README.md](a3/README.md)
    - Create Entity Relationship Diagram (ERD) for Pet's R-Us
    - Provide screenshots of ERD
    - Provide screenshots of populated tables
    - Upload mwb and sql files
    - Upload image of a3/index.jsp
  
4. [A4 README.md](a4/README.md)
    - Show server side validation setup
    - Show successful server side validation
    - Show unsuccessful server side validation
- Complete skillsets 10-12
  
5. [A5 README.md](a5/README.md)
    - Add insert functionality to web form
    - Show successful insertion into local database
    - Complete skillsets 13-15

*Projects*

1. [P1 README.md](p1/README.md)
    - Add jQuery validation to p1/index.jsp
    - Use regexp to only allow certain characters for specific forms
    - Use HTML5 to limit number of characters for each form

2. [P2 README.md](p2/README.md)
    - Show completed CRUD functionality
    - Show tables on site
    - Show updated database using webapp