# LIS4368 - Advanced Web Application Development

## Troy Noble

### Project #2 Requirements:

1. Complete CRUD functionality for website
2. Show updated databases
3. Show functional site pages

#### README.md file should include the following items:

* Screenshots of different parts of site
* Screenshots of updated database

#### Assignment Screenshots:

*Table view*

![](img/table_view.png)

| Initial Add              | Successful Add           |
| ------------------------ | ------------------------ |
| ![](img/initial_add.png) | ![](img/add_success.png) |

| Initial Update                            | Successful Update                         |
| ----------------------------------------- | ----------------------------------------- |
| ![Update initial](img/initial_update.png) | ![Update success](img/update_success.png) |

*Delete functionality*

![Delete functionality](img/delete.png)

*DB Updates*

![DB Updates](img/db_updates.png)