# LIS4368 - Advanced Web Application Development

## Troy Noble

### Assignment #3 Requirements:

*Deliverables*

1. Entity Relationship Diagram (ERD)
2. Include data (at least 10 records each table)
3. Provide Bitbucket read-only access to A3 repo (Language SQL), *must* include README.md, using Markdown syntax, and include links to *all* of the following items (from README.md):
    - docs folder: a3.mwb, and a3.sql
    - img folder: a3.png (export a3.mwb file as a3.png)
    - README.md (*MUST* display a3.png ERD)

#### README.md file should include the following items:

* Screenshot of A3 ERD that links to the image

  

#### Assignment Screenshots and Links:

*Screenshot of A3 ERD:*

![A3 ERD](img/a3.png "ERD for A3")

*Screenshot A3/index.jsp*

![A3 Index](img/a3index.png "A3 Index page on LocalHost")

*Populated Pet table*

![Pet Table](img/a3_pet.png "Table for Pet")

*Populated Customer table*

![Customer Table](img/a3_customer.png "Table for customers")

*Populated Petstore table*

![Petstore table](img/a3_petstore.png "Table for Petstores")

*A3 docs: a3.mwb and a3.sql:*

[A3 MWB File](docs/a3.mwb)

[A3 SQL File](docs/a3.sql)